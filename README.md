# YFROBOT I2C Expander
 
![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 简介 Introduction

本扩展库为Mind+软件设计。

支持YFROBOT I2C 扩展器，解决I2C地址冲突问题，地址相同器件可一起时可用。

## 相关链接 Links
* 本项目加载链接: https://github.com/YFROBOT-TM

* 产品购买链接: [YFROBOT商城](https://www.yfrobot.com/)、[YFROBOT淘宝商城](https://yfrobot.taobao.com/).


## 积木列表 Blocks
![](./arduinoC/_images/blocks.png)


## 示例程序 Examples
![](./arduinoC/_images/example.png)


## 许可证 License
MIT


## 硬件支持列表 Hardware Support
主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
arduino uno        |             |       √已测试       |             |
micro:bit        |             |       √未测试       |             | 
mpython掌控板        |             |        √未测试      |             | 
ESP32E        |             |       √未测试        |             | 


## 更新日志 Release Note

* V0.0.3  限制检测范围2~400cm，检测错误时，返回-1，应用时判断排除-1；Mind+V1.8.0 RC3.1版本软件测试 20250304
* V0.0.2  修复错误，wire初始化，Mind+V1.7.3 RC3.0版本软件测试
* V0.0.1  基础功能完成，Mind+V1.7.3 RC1.0版本软件测试


## 联系我们 Contact Us
* http://www.yfrobot.com.cn/wiki/index.php?title=%E8%81%94%E7%B3%BB%E6%88%91%E4%BB%AC

## 其他扩展库 Other extension libraries
* http://yfrobot.com.cn/wiki/index.php?title=YFRobot%E5%BA%93_For_Mind%2B

## 参考 Reference Resources