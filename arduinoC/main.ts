/** 
 * @file yfrobot
 * @brief YFROBOT's sensors Mind+ library.
 * @n This is a MindPlus graphics programming extension for YFROBOT's module.
 * 
 * @copyright    YFROBOT,2022
 * @copyright    MIT Lesser General Public License
 * 
 * @author [email](yfrobot@qq.com)
 * @date  2023-03-07
*/

//% color="#ff6f0c" iconWidth=50 iconHeight=40
namespace i2cSonar {

    //% block="Read I2C Sonar(cm)" blockType="reporter"
    export function readSonarI2C(parameter: any, block: any) {
        
        Generator.addInclude(`include_Wire`, `#include <Wire.h>`);
        Generator.addInclude(`definereadSonarI2C`, `float readSonar(); //预定义函数readSonar`)
        Generator.addInclude(`definereadSonarI2CFun`, ``+
            `float readSonar() {\n`+
            `  float distance = 0;\n`+
            `  float ds[3] = {0, 0, 0};\n`+
            `  uint8_t i = 0;\n`+
            `  Wire.beginTransmission(0x57);\n`+
            `  Wire.write(1);\n`+
            `  Wire.endTransmission();\n`+
            `  delay(110);\n\n`+
            `  Wire.requestFrom(0x57, 3);\n`+
            `  while (Wire.available()) {\n`+
            `    ds[i++] = Wire.read();\n`+
            `  }\n`+
            `  distance = (ds[0] * 65536 + ds[1] * 256 + ds[2]) / 10000; //计算成CM值\n`+
            `  if ((distance > 2) && (distance <= 400)) { \n`+
            `    return distance;\n`+
            `  } \n`+
            `  delay(30);\n`+
            `  return -1; // 返回 -1 表示数据错误\n`+
            `}`
        );
        Generator.addSetup(`setup_Wire_begin`, `Wire.begin();`);

        Generator.addCode(`readSonar()`);

    }
}
